package com.qurius.uima.annotator;

import com.qurius.uima.annotations.DocumentScore;
import com.qurius.uima.annotations.Phrase;
import org.apache.commons.lang.StringUtils;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.fit.descriptor.OperationalProperties;
import org.apache.uima.fit.descriptor.ResourceMetaData;
import org.apache.uima.fit.descriptor.TypeCapability;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by buddha on 11/3/15.
 */
@ResourceMetaData(
        name = "Weighted Dictionary Annotator",
        description = "The Weighted Dictionary provides Breen style Sentiment analysis via weighted dictionary lookup.",
        vendor = "Qurius",
        version = "1.1",
        copyright = "ASF"
)
@OperationalProperties(
        modifiesCas = true,
        outputsNewCases = false
)
@TypeCapability(outputs = {"com.qurius.uima.annotations.Phrase", "com.qurius.uima.annotations.DocumentScore"})
public class Dictionary extends JCasAnnotator_ImplBase {
    private static final Logger log = LoggerFactory.getLogger(Dictionary.class);

    public static final String PARAM_WORD_FORMS = "dictionaryLoc";
    @ConfigurationParameter(name = PARAM_WORD_FORMS, description = "The location of the weighted dictionary, a line separated file of the form term|value.")
    public String dictionaryLoc;

    public static final String PARAM_DICT_ID = "dictionaryId";
    @ConfigurationParameter(name = PARAM_DICT_ID, description = "Dictionary's ID.", mandatory = false)
    public String dictionaryId;

    public static final String PARAM_DICT_NAME = "dictionaryName";
    @ConfigurationParameter(name = PARAM_DICT_NAME, description = "Dictionary's Name.", mandatory = false)
    public String dictionaryName;

    public static final String PARAM_PHRASE = "phrases";
    @ConfigurationParameter(name = PARAM_PHRASE, description = "Add Phrase annotation to the document.", mandatory = false, defaultValue = "true")
    public boolean phrases;

    public HashMap<String, String> dictionary = new HashMap<String, String>();

    private Pattern keyPattern;

    public void initialize(UimaContext aContext) throws ResourceInitializationException {
        super.initialize(aContext);

        dictionary.clear();
        keyPattern = null;

        if (dictionaryName == null) {
            File f = new File(dictionaryLoc);
            dictionaryName = f.getName();
        }

        try {
            String line;
            ArrayList<String> patternsList = new ArrayList<String>();
            BufferedReader reader = Files.newBufferedReader(Paths.get(dictionaryLoc), StandardCharsets.UTF_8);
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split("\\|");
                String word = parts[0].trim();
                String weight = parts[1].trim();
                dictionary.put(word, weight);
                patternsList.add(Pattern.quote(word));
            }
            keyPattern = Pattern.compile("\\b" + StringUtils.join(patternsList, "\\b|\\b") + "\\b");
            reader.close();
        } catch (Exception e) {
            System.err.print("Cannot access word forms file");
            throw new RuntimeException(e);
        }
    }

    @Override
    public void process(JCas aJCas) throws AnalysisEngineProcessException {
        double score = 0;

        if (dictionary.size() > 0) {
            String content = aJCas.getDocumentText();
            if (content == null) throw new RuntimeException("aJCas.getDocumentText() is null.");

            Matcher m = keyPattern.matcher(content);
            while (m.find()) {

                String group = m.group();
                score += Double.valueOf(dictionary.get(group));

                if (phrases) {
                    Phrase annotation = new Phrase(aJCas);
                    annotation.setLabel(group);
                    annotation.setScore(dictionary.get(group));
                    annotation.setDictionaryId(dictionaryId);
                    annotation.setBegin(m.start());
                    annotation.setEnd(m.end());
                    annotation.addToIndexes();
                }
            }
        }

        DocumentScore documentScore = new DocumentScore(aJCas);
        documentScore.setDictionaryId(dictionaryId);
        documentScore.setDictionaryName(dictionaryName);
        documentScore.setScore(score);
        documentScore.addToIndexes();
    }
}
