# README

If you can read this you are too close.

# Qubla

This is qubla compatible

## Test Suite

I have included some DTOs in the test suite for convenience. These help link the annotator to Qubla a bit better.

## Qubla Runnotate

To test this using the Qubla Maven plugin, use

```
mvn -f
  -DgroupId=com.qurius.uima \
  -DartifactId=annotator.dictionary \
  -Dversion=1.0 \
  -DdocumentJSON='{"documentText":"Hillary Clinton and Bill were in the United States of America recently.", "language":null}'}' \
  -DannotatorJSON='{}' \
  com.qurius:annotator-runner:runnotate
```

## Runnotator

Much of the test suite replicates the Qurius "Runnotator" class and may seem superfluous to the casual user.
